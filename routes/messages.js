import express from 'express'
import { verifyToken } from '../middleware/auth.js'
import { addMessage, getMessage } from '../controllers/messages.js'

const router = express.Router()

router.post('/', verifyToken, addMessage)

router.get('/:conversationId', verifyToken, getMessage)

export default router
