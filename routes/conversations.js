import express from 'express'
import { verifyToken } from '../middleware/auth.js'
import {
  getConversationBetweenUsers,
  getUserConversations,
  newConversation,
} from '../controllers/conversations.js'

const router = express.Router()
//new conv

router.post('/', verifyToken, newConversation)

//get conv of a user

router.get('/:userId', verifyToken, getUserConversations)

// get conv includes two userId

router.get(
  '/find/:firstUserId/:secondUserId',
  verifyToken,
  getConversationBetweenUsers
)

export default router
