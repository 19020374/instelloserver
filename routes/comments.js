import express from 'express';
import { getComment, insertComment, updateComment, deleteComment } from '../controllers/comments.js';
import { verifyToken } from '../middleware/auth.js';

const router = express.Router();

router.get('/getComments/:postId', verifyToken, getComment)
router.patch('/:postId/insertComment', verifyToken, insertComment)
router.post('/:postId/:commentId/updateComment', verifyToken, updateComment)
router.post('/:postId/:commentId/deleteComment', verifyToken, deleteComment)

export default router