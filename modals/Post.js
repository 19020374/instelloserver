import mongoose from "mongoose";

const postSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
      required: true,
    },
    firstName: {
      type: String,
      required: true,
      min: 2,
      max: 50,
    },
    lastName: {
      type: String,
      required: true,
      min: 2,
      max: 50,
    },
    picturePath: {
      type: String,
      default: "",
    },
    userPicturePath: {
      type: String,
      default: "",
    },
    location: String,
    description: String,
    likes: {
      type: Map,
      of: Boolean,
    },
    comments: [
      {
        _id: {
          type: String,
          required: true,
        },
        postId: {
          type: String,
          required: true,
        },
        userId: {
          type: String,
          required: true,
        },
        userPicturePath: {
          type: String,
          default: "",
        },
        userFirstName: {
          type: String,
          required: true,
        },
        userLastName: {
          type: String,
          required: true,
        },
        content: {
          type: String,
          required: true,
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

const Post = mongoose.model("Post", postSchema);
export default Post;
