import Conversation from "../modals/Conversation.js";

// NEW CONVERSATION
export const newConversation = async (req, res) => {
  const newConversation = new Conversation({
    members: [
      {
        id: req.body[0].id,
        firstName: req.body[0].firstName,
        lastName: req.body[0].lastName,
        picturePath: req.body[0].picturePath,
      },
      {
        id: req.body[1].id,
        firstName: req.body[1].firstName,
        lastName: req.body[1].lastName,
        picturePath: req.body[1].picturePath,
      },
    ],
  });

  try {
    const savedConversation = await newConversation.save();
    res.status(200).json(savedConversation);
  } catch (err) {
    res.status(500).json(err);
  }
};

// GET USER ALL CONVERSATION
export const getUserConversations = async (req, res) => {
  const { userId } = req.params;

  try {
    const conversations = await Conversation.find({
      members: {
        $elemMatch: { id: userId },
      },
    });

    res.status(200).json(conversations);
  } catch (err) {
    res.status(500).json(err);
  }
};

// GET CONVERSATION BETWEEN 2 USERS
export const getConversationBetweenUsers = async (req, res) => {
  const { firstUserId, secondUserId } = req.params;

  try {
    const conversation = await Conversation.find({
      "members.id": { $all: [firstUserId, secondUserId] },
    });
    res.status(200).json(conversation);
  } catch (err) {
    res.status(500).json(err);
  }
};
