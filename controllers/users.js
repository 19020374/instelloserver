import User from '../modals/User.js'

/* READ */
export const getUser = async (req, res) => {
  console.log(req)
  try {
    const { id } = req.params
    const user = await User.findById(id)
    res.status(200).json(user)
  } catch (error) {
    res.status(404).json({ message: error.message })
  }
}

export const getUserFriends = async (req, res) => {
  try {
    const { id } = req.params
    const user = await User.findById(id)

    const friends = await Promise.all(
      user.friends.map((id) => User.findById(id))
    )

    const formattedFriends = friends.map(
      ({ _id, firstName, lastName, occupation, location, picturePath }) => {
        return { _id, firstName, lastName, occupation, location, picturePath }
      }
    )

    res.status(200).json(formattedFriends)
  } catch (error) {
    res.status(404).json({ message: error.message })
  }
}

export const getUserByName = async (req, res) => {
  try {
    var userName = req.body.search
    console.log(userName)
    var searchString = new RegExp(userName, 'ig')

    const user = await User.aggregate()
      .project({
        fullname: { $concat: ['$firstName', ' ', '$lastName'] },
        firstName: 1,
        lastName: 1,
        username: 1,
        email: 1,
        picturePath: 1,
        friends: 1,
        location: 1,
        occupation: 1,
      })
      .match({ fullname: searchString })

    res.status(200).json(user)
  } catch (error) {
    res.status(404).json({ message: error.message })
  }
}

/*  UPDATE */
export const addRemoveFriend = async (req, res) => {
  try {
    const { id, friendId } = req.params
    const user = await User.findById(id)
    const friend = await User.findById(friendId)

    if (user.friends.includes(friendId)) {
      user.friends = user.friends.filter((id) => id !== friendId)
      friend.friends = friend.friends.filter((id) => id !== id)
    } else {
      user.friends.push(friendId)
      friend.friends.push(id)
    }

    await user.save()
    await friend.save()

    const friends = await Promise.all(
      user.friends.map((id) => User.findById(id))
    )

    const formattedFriends = friends.map((friend) => {
      return friend
    })

    res.status(200).json(formattedFriends)
  } catch (error) {
    res.status(404).json({ message: error.message })
  }
}
