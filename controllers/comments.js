import mongoose from "mongoose";
import Post from "../modals/Post.js";

export const getComment = async (req, res) => {
  try {
    const { postId } = req.params;
    const post = await Post.findById(postId);
    const comments = post.comments;
    console.log(comments);
    res.status(200).json(comments);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const insertComment = async (req, res) => {
  try {
    const { postId } = req.params;
    const comment = req.body;

    const newComment = {
      _id: new mongoose.Types.ObjectId(),
      postId: postId,
      userId: comment.userId,
      userPicturePath: comment.userPicturePath,
      userFirstName: comment.userFirstName,
      userLastName: comment.userLastName,
      content: comment.content,
    };

    const post = await Post.findById(postId);
    post.comments.push(newComment);

    await post.save();
    res.status(201).json(post);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const updateComment = async (req, res) => {
  try {
    const { postId, commentId } = req.params;

    const post = await Post.findById(postId);

    const oldComment = post.comments.find(
      (comment) => comment._id === commentId
    );

    const oldCommentIndex = post.comments.findIndex(
      (comment) => comment._id === commentId
    );

    const updatedComment = {
      _id: oldComment._id,
      postId: postId,
      userId: oldComment.userId,
      userPicturePath: oldComment.userPicturePath,
      userFirstName: oldComment.userFirstName,
      userLastName: oldComment.userLastName,
      content: req.body.content,
    };

    post.comments.splice(oldCommentIndex, 1);
    post.comments.push(updatedComment);

    post.save()

    res.status(201).json(post);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const deleteComment = async (req, res) => {
  try {
    const { postId, commentId } = req.params;

    const post = await Post.findById(postId)

    const deleteCommentIdx = post.comments.findIndex(comment => comment._id === commentId)

    post.comments.splice(deleteCommentIdx, 1)
    post.save()

    res.status(201).json(post);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};
